# microservicio-cloud-config-server


[Spring cloud config server ](https://howtodoinjava.com/spring-cloud/spring-cloud-config-server-git/)

Spring Cloud Config Server with Git Integration
By Sajal Chakraborty | Filed Under: Spring Cloud

Now-a-days we often come across the term Microservices while discussing any new java based server side development (mainly service API). Microservices approach now has become an industry standard for any new API development and almost all the organizations are promoting it.

In this tutorial, we will discuss and do a demo on a specific Microservice feature called Config Server. Config server is where all configurable parameters of microservices are written and maintained. It is more like externalizing properties / resource file out of project codebase to an external service altogether, so that any changes to that property does not necessitate the deployment of service which is using the property. All such property changes will be reflected without redeploying the microservice